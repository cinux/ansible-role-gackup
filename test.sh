#!/bin/bash
which virtualenv > /dev/null
if [ $? -ne 0 ]
then
	echo "Virtualenv is missing, please install it."
	exit 1
fi
if [ ! -d ".env" ]
then
	echo "virtualenv not exist, creating..."
	virtualenv .env
	source .env/bin/activate
	echo " virtualenv created and entered, install molecule"
	pip install molecule[docker] molecule-inspec

else
	source .env/bin/activate
fi
molecule test
molecule destroy
deactivate
