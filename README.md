Role Name
=========

This role deploys the [gackup](https://codeberg.org/cinux/gackup) python script to mirror git repositorys from source to target.

Requirements
------------

none

Role Variables
--------------

| name | description |
|:---  | :---        |
| ansible_role_gackup_repo_url | necessary to pull the gackup repository from git |
| ansible_role_gackup_list | contains a list of source and target pares |
| 	name | meaningful name |
|	source_url | git source URL from where to mirror |
|	target_url | git target URL where to mirror |

Dependencies
------------

none

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - { role: username.rolename, x: 42 }

License
-------

MIT

Author Information
------------------

https://codeberg.org/cinux
