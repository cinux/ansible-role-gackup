# frozen_string_literal: true
# color: false

# Molecule managed

describe file('/etc/gackup-list.yml') do
  its('owner') { should eq 'root' }
  its('group') { should eq 'root' }
  its('mode') { should cmp '0755' }
  its('content') { should match [
    '# example',
    '#- name: codeberg dotfiles',
    '#  source_url: https://codeberg.org/cinux/dotfiles',
    '#  target_url: git@gitlab.com:cinux/backup/dotfiles.git',
    '- name: codeberg dotefiles',
    '  source_url: https://codeberg.org/cinux/dotfiles',
    '  target_url: git@gitlab.invisibleworld.de:backup/codeberg/dotfiles.git',
    '- name: codeberg',
    '  source_url: https://codeberg.org/cinux/gackup',
    '  target_url: git@gitlab.invisibleworld.de:backup/codeberg/gackup.git'
    ].join("\n")
  }
end

describe file('/usr/local/bin/gackup.py') do
  its('owner') { should eq 'root' }
  its('group') { should eq 'root' }
  its('mode') { should cmp '0755' }
end

describe file('/etc/systemd/system/gackup.service') do
  its('owner') { should eq 'root' }
  its('group') { should eq 'root' }
  its('mode') { should cmp '0755' }
end

describe file('/etc/systemd/system/gackup.timer') do
  its('owner') { should eq 'root' }
  its('group') { should eq 'root' }
  its('mode') { should cmp '0755' }
end

describe file('/tmp/gackup') do
  it { should_not exist }
end
